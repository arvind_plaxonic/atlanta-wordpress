<footer>
    <div class="container">
      <article class="footTop">
        <div class="row align-items-center">
          <div class="col-md-5 text-center">
            <!-- <ul class="d-inline footer_link">
              <li class="d-inline pr-4"><a href="#">Physicians</a></li>
              <li class="d-inline pr-4"><a href="#">Attorneys</a></li>
              <li class="d-inline pr-4"><a href="#">Patients</a></li>
              <li class="d-inline pr-4"><a href="#">Other Providers</a></li>
              <li class="d-inline"><a href="#">Contact us</a></li>
            </ul> -->

            <?php   wp_nav_menu( array(
		                                    'menu'    => 'Footer menu',
		                                    'depth'             => 1,
		                                    //'container'         => 'li',
		                                    //'container_class'   => 'test',
		                                    //'container_id'      => 'test',
		                                    'menu_class'        => 'd-inline footer_link',
		                                    //'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
		                                    'walker'            => new WP_Bootstrap_Navwalker())
		                                );
		                                ?>


          </div>
          <div class="col-md-3 text-center">
            <a href="<?php echo get_site_url(); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/footer-logo.png" alt="Footer-Logo"></a>
          </div>

          <div class="col-md-4 text-center">
            <ul>
              <li class="d-inline mr-4">Follow us on social</li>      
              <?php
                        if (is_active_sidebar('sidebar-1')) : //check the sidebar if used.
                          dynamic_sidebar('sidebar-1');  // show the sidebar.
                        endif;
                        ?>       
              <!-- <li class="d-inline mr-4 pr-3"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
              <li class="d-inline mr-4 pr-3"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
              <li class="d-inline mr-4 pr-3"><a href="#"><i class="fab fa-twitter"></i></a></li>
              <li class="d-inline"><a href="#">
                  <?xml //version="1.0" encoding="windows-1252"?>               
                  <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 26 26"
                    style="enable-background:new 0 0 26 26;" xml:space="preserve">
                    <g>
                      <path
                        d="M20,7c-0.551,0-1-0.449-1-1V4c0-0.551,0.449-1,1-1h2c0.551,0,1,0.449,1,1v2c0,0.551-0.449,1-1,1H20z   " />
                      <path
                        d="M13,9.188c-0.726,0-1.396,0.213-1.973,0.563c0.18-0.056,0.367-0.093,0.564-0.093   c1.068,0,1.933,0.865,1.933,1.934c0,1.066-0.865,1.933-1.933,1.933s-1.933-0.866-1.933-1.933c0-0.199,0.039-0.386,0.094-0.565   C9.4,11.604,9.188,12.274,9.188,13c0,2.107,1.705,3.813,3.813,3.813c2.105,0,3.813-1.705,3.813-3.813S15.105,9.188,13,9.188z" />
                      <g>
                        <path
                          d="M13,7c3.313,0,6,2.686,6,6s-2.688,6-6,6c-3.313,0-6-2.686-6-6S9.687,7,13,7 M13,5    c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S17.411,5,13,5L13,5z" />
                      </g>
                      <path
                        d="M21.125,0H4.875C2.182,0,0,2.182,0,4.875v16.25C0,23.818,2.182,26,4.875,26h16.25   C23.818,26,26,23.818,26,21.125V4.875C26,2.182,23.818,0,21.125,0z M24,9h-6.537C18.416,10.063,19,11.461,19,13   c0,3.314-2.688,6-6,6c-3.313,0-6-2.686-6-6c0-1.539,0.584-2.938,1.537-4H2V4.875C2,3.29,3.29,2,4.875,2h16.25   C22.711,2,24,3.29,24,4.875V9z" />
                    </g>
                  
                  </svg>
                </a></li> -->
            </ul>
          </div>
        </div>
      </article>

      <article class="footBottom text-center">
     
        <p>Copyright &copy 2021. Powered by <a href="https://www.plaxonic.com/" target="_blank">Plaxonic</a></p>
      </article>
    </div>
</footer>


  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery-3.3.1.min.js"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bootstrap.min.js"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/humburger-menu.js"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/slick.min.js"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/aos.js"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/custom.js"></script>

  <script>
    $(document).ready(function () {
      AOS.init({
        once: true
      });
    });
  </script>

</body>

</html>