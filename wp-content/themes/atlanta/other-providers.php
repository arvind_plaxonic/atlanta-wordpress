<?php /* Template Name: Other Providers */
get_header();
?>

<?php

//Page Post 1
$page_post1 = get_post(106);
$page_post1_title = $page_post1->post_title;
$page_post1_content = $page_post1->post_content;
$page_post1_img = get_the_post_thumbnail_url($page_post1, 'thumbnail');

//Page Post 2
$page_post2 = get_post(107);
$page_post2_title = $page_post2->post_title;
$page_post2_content = $page_post2->post_content;
$page_post2_img = get_the_post_thumbnail_url($page_post2, 'thumbnail');

?>
<!--Banner Part-->

<?php
$image_url = wp_get_attachment_url(get_post_thumbnail_id());
if (!empty(get_the_post_thumbnail())) {
?>
  <section class="atlanta_common" style="background-image:url('<?php echo $image_url; ?>">
    <div class="container">
      <div class="header_banner-cont">
        <div class="page-title-inner">
          <h2 class="page-title"><?php echo get_the_title(); ?></h2>
          <ul class="banner-breadcrumb">
            <li><a class="breadcrumb-entry pr-2" href="<?php echo get_site_url(); ?>">Home</a></li>
            <li><span class="breadcrumb-entry"><?php echo get_the_title(); ?></span></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
<?php } else { ?>
  <section class="atlanta_common" style="background-image:url('<?php echo esc_url(get_template_directory_uri()); ?>/images/attorney_banner.jpg');">
    <div class="container">
      <div class="header_banner-cont">
        <div class="page-title-inner">
          <h2 class="page-title"><?php echo get_the_title(); ?></h2>
          <ul class="banner-breadcrumb">
            <li><a class="breadcrumb-entry pr-2" href="<?php echo get_site_url(); ?>">Home</a></li>
            <li><span class="breadcrumb-entry"><?php echo get_the_title(); ?></span></li>
          </ul>
        </div>
      </div>
    </div>
  </section>

<?php } ?>
<!--Content Start-->
<section class="Patients-content-section atl_cont_box">
  <div class="container">
    <div class="atorim_fullcondiv">
      <div class="row">

        <div class="col-md-6">
          <div class="health_care">
            <img src="<?php echo $page_post1_img; ?>" alt="hospital-center">
          </div>
        </div>

        <div class="col-md-6">
          <div class="atorim_common_content pl-4">
            <div class="atorim_common_para patient_para2">
              <div class="atorim_common_para">
                <h3><?php echo $page_post1_title; ?></h3>
              </div>
              <div class="paraStyle">
                <?php echo $page_post1_content; ?>
              </div>


            </div>
          </div>
        </div>

        <div class="col-md-6 order-1 mt-5">
          <div class="health_care">
            <img src="<?php echo $page_post2_img; ?>" alt="Attroney-image">
          </div>
        </div>
        <div class="col-md-6 mt-5">
          <div class="atorim_common_content patient_content3">
            <div class="atorim_common_para">
              <h3><?php echo $page_post2_title; ?></h3>
            </div>
            <div class="atorim_common_para patient_para3">
              <div class="paraStyle">
                <?php echo $page_post2_content; ?>
              </div>

            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>