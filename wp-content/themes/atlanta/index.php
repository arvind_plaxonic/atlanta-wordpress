<?php /* Template Name: index Page */
get_header();


?>


<!--Banner Part-->

<section class="atlanta_common physician-banner" style="background-image:url('<?php echo esc_url(get_template_directory_uri()); ?>/images/attorney_banner.jpg');">
  <div class="container">
    <div class="header_banner-cont">
      <div class="page-title-inner">
        <h2 class="page-title">Blog<?php //echo get_the_title(); 
                                    ?></h2>
        <ul class="banner-breadcrumb">
          <li><a class="breadcrumb-entry pr-2" href="<?php echo get_site_url(); ?>">Home</a></li>
          <li><span class="breadcrumb-entry"><?php echo get_the_title(); ?></span></li>
        </ul>
      </div>
    </div>
  </div>
</section>

<!--********** -->



<section class="single-blog-sec1 py-5 mt-md-4 mt-0 mb-4 mb-md-0">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-md-12 col-xxl-11">
        <div class="single_blog_post">
          <div class="row">

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <?php $postid = get_the_ID();

                $category_detail = get_the_category($postid); //$post->ID
                foreach ($category_detail as $cd) {
                  //print_r($cd);
                  $id = $cd->ID;
                }
                ?>

                <div class="col-md-12 col-lg-8">
                  <div class="full_post">
                    <div class="blog_box blog_style2">
                      <div class="blog_title p-0">
                        <div class="d-flex align-items-center mb-2">
                         
                          <p><?php echo get_the_date(); ?></p>
                        </div>
                   

                      </div>
                      <article class="aboutRight">
                        <div class="headDiv pb-4">
                          <h2 class="pb-5">
                          <?php the_title(); ?>
                          </h2>
                        </div>
                        <div class="feacture_img pt-4">
                        <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="img-fluid">
                      </div>
                        <div class="paraStyle"><?php echo get_the_content(); ?></div>


                       
                      </article>
                      
                      

                     

                    </div>
                  </div>
                </div>

              <?php endwhile;
            else : ?>
              <p>
                <?php _e('Sorry, no posts matched your criteria.'); ?>
              </p>
            <?php endif; ?>

            <div class="col-lg-4 col-md-12 mt-4 mt-lg-0">
              <div class="recent_blogs ms-xxl-3 ms-xl-1">
                <h2 class="mb-0">Other Post</h2>
                <div class="other_blog_box">
                  <?php $posts = new WP_Query(array('post_type' => 'post', 'category_name' => 'blog', 'order' => 'ASC', 'posts_per_page' => '5')); ?>

                  <?php while ($posts->have_posts()) : $posts->the_post(); ?>

                    <div class="blog_box blog_style2">
                      <a href="<?php the_permalink(); ?>">
                        <div class="img_box">
                          <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="img-fluid">
                        </div>

                        <div class="blog_title">
                          <div class="d-flex align-items-center mb-1">
                           
                            <p><?php echo get_the_date(); ?></p>
                          </div>
                          <h3><?php echo wp_trim_words(get_the_title(), 10, '...'); ?></h3>
                        </div>
                      </a>
                    </div>
                  <?php endwhile; ?>
                  <!-- <div class="blog_box blog_style2">
										<a href="javascript:void(0)">
											<div class="img_box">
												<img src="images/blog6.jpg" class="img-fluid">
											</div>

											<div class="blog_title">
												<div class="d-flex align-items-center mb-1">
													<span>SEM</span>
													<p>July 27, 2021</p>
												</div>
												<h3>Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</h3>
											</div>
										</a>
									</div>
									<div class="blog_box blog_style2">
										<a href="javascript:void(0)">
											<div class="img_box">
												<img src="images/blog8.jpg" class="img-fluid">
											</div>

											<div class="blog_title">
												<div class="d-flex align-items-center mb-1">
													<span>SEO</span>
													<p>July 27, 2021</p>
												</div>
												<h3>Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</h3>
											</div>
										</a>
									</div>
									<div class="blog_box blog_style2">
										<a href="javascript:void(0)">
											<div class="img_box">
												<img src="images/blog9.jpg" class="img-fluid">
											</div>

											<div class="blog_title">
												<div class="d-flex align-items-center mb-1">
													<span>SEM</span>
													<p>July 27, 2021</p>
												</div>
												<h3>Doloremque laudantium, totam rem aperiam, eaque ipsa quae.</h3>
											</div>
										</a>
									</div> -->
                </div>
              </div>

            </div>


          </div>


        </div>
      </div>

    </div>
  </div>
</section>
<?php get_footer(); ?>