
/*------- banner slider --------*/
// $(document).ready(function() {
//   var owl = $('.jumbotron .owl-carousel');
//   owl.owlCarousel({
// 	animateOut: 'fadeOut',
// 	margin: 10,
// 	nav: true,
// 	dots: false,
// 	autoplay: true,
// 	loop: true,
// 	responsive: {
// 	  0: {
// 		items: 1
// 	  },
// 	  600: {
// 		items: 1
// 	  },
// 	  1000: {
// 		items: 1
// 	  }
// 	}
//   })
// })
/*------- banner slider --------*/

/*------- header scroll --------*/
$(document).ready(function(){       
   
	$(window).scroll(function() {
		var scroll = $(window).scrollTop();
			if(scroll>1){
				$('header').addClass('Animeheader');
				$('.jumbotron').addClass('topSpace');
		}
		
		else {
			$('header').removeClass('Animeheader');
			$('.jumbotron').removeClass('topSpace');
		}
	});
	
});
/*------- header scroll --------*/

/*------- slick slider --------*/
$(document).ready(function(){ 
	$('.jumbotron .slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		arrows: true,
		fade: true
	 });
});
/*------- slick slider --------*/